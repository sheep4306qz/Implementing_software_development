// Example 9-1: Virtual Function
// [ Code ] : virtfunc.cpp
#include <iostream>
#include <cstdlib>
using namespace std;
class Bird {
 public: // 將 move() 宣告為虛擬函式，利用在父類別加上virtual來讓程式利用動態繫結來進行分辨多型行為	(polymorphic	behavior)。
 virtual void move() { cout <<"Bird can fly.\n"; }
};
class Goose:public Bird {
 public:
 virtual void move() { cout <<"Goose can fly, run and swim.\n"; }
};
class Penguin:public Bird {
 public:
 virtual void move() { cout <<"Penguin can run and swim.\n"; }
};
 int main() {
 Bird bird;
 Goose goose;
 Penguin penguin;
 Bird *p;
 p = &bird;
 p->move();
 p = &goose;
 p->move();
 p = &penguin;
 p->move();

 system("pause");
 return 0;
}

