// Example 9-1: Virtual Function
// [ Code ] : virtfunc.cpp
#include <iostream>
#include <cstdlib>
using namespace std;
class Point {
 public: // 將 Draw() 宣告為虛擬函式，利用在父類別加上virtual來讓程式利用動態繫結來進行分辨多型行為	(polymorphic	behavior)。

 virtual void Draw() { cout <<"Draw a point.\n"; }
};
class Square:public Point {
 public:
  virtual void Draw() { cout <<"Draw a square.\n"; }
};
class Cube:public Square {
 public:
  virtual void Draw() { cout <<"Draw a cube.\n"; }
};
 int main() {
 Point point;
 Square square;
 Cube cube;
 Point *p;
 p = &point;
 p->Draw();
 p = &square;
 p->Draw();
 p = &cube;
 p->Draw();

 system("pause");
 return 0;
}
