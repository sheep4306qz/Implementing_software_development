// [ Code ]: ex8-1a.cpp -- 衍⽣類別的建構⼦和解構⼦
#include <iostream>
#include <cstdlib>
using namespace std;
class BASE{
public:
 BASE(){cout << "B1 "; }
 BASE(BASE & refB){cout << "B2 "; } // Copy Constructor
 BASE(const BASE & refB){cout << "B3 "; }
 void func(int=0){cout << "B4 "; }
 ~BASE(){cout << "B5 "; }
};
class DERIVED : public BASE{
public:
 DERIVED(){ cout << "D1 ";}
 DERIVED(DERIVED & refB){cout << "D2 "; } // Copy Constructor
 DERIVED(const DERIVED & refB){cout << "D3 "; }
 void func(int=0){cout << "D4 "; }
 ~DERIVED(){cout << "D5 "; }
};
class DERIVED2 : public DERIVED{
public:
 DERIVED2(){ cout << "DD1 ";}
 DERIVED2(DERIVED2 & refB){cout << "DD2 "; } // Copy Constructor
 DERIVED2(const DERIVED2 & refB){cout << "DD3 "; }
 void func(int=0){cout << "DD4 "; }
 ~DERIVED2(){cout << "DD5 "; }
};
int main(){
 cout << "Running with BASE Class : ";
 BASE b1;
 const BASE b2(b1); // 呼叫 BASE’s Copy Constructor
 BASE b3(b2); // 呼叫 BASE’s Copy Constructor
 b1.func();
 cout << endl << endl;
 cout << "Running with DERIVED Class : ";
 DERIVED d1;
 const DERIVED d2, d3(d1); // 呼叫 DERIVED’s Copy Constructor
 DERIVED d4(d2); // 呼叫 DERIVED’s Copy Constructor
 d1.func();
 cout << endl << endl;
 cout << "Running with DERIVED2 Class : ";
 DERIVED2 dd1;
 const DERIVED2 dd2, dd3(dd1); // 呼叫 DERIVED2’s Copy Constructor
 DERIVED2 dd4(dd2); // 呼叫 DERIVED2’s Copy Constructor
 dd1.func();
  cout << "\n\nDelete all objects : ";
 delete &dd1;
 delete &dd2;
 delete &dd3;
 delete &d1;
 delete &d2;
 delete &d3;
 delete &d4;
 delete &b1;
 delete &b2;
 delete &b3;
 cout << endl << endl;
 system("PAUSE");
 return 0;
}
