// [ Code ]: ex8-2.cpp �X �h���~��
#include <iostream>
#include <cstdlib>
using namespace std;
class Base_A{
public:
 Base_A(){cout << "=> Base_A "; }
 ~Base_A(){cout << "=> ~Base_A "; } };
class Base_B{
public:
 Base_B(){cout << "=> Base_B "; }
 ~Base_B(){cout << "=> ~Base_B "; } };
class Derived_C : public Base_B, public Base_A {
public:
 Derived_C(){cout << "=> Derived_C "; }
 ~Derived_C(){cout << "=> ~Derived_C "; } };
int main(){
 cout << "Creating Derived_C's object, DC ..." << endl;
 cout << "Loading Default Constructors' Sequence ";
 Derived_C DC;
 cout << endl << endl;
 cout << "Deleting Derived_C's object, DC ..." << endl;
 cout << "Loading Default Destructors' Sequence ";
 delete &DC;
 cout << endl << endl;
 system("PAUSE");
 return 0;
}
