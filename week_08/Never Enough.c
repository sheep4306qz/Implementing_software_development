#include<stdio.h>
int main(void){
    int i,j,narray,loop;
    char array[21][50]={"I'm trying to hold my breath",
    "Let it stay this way",
    "Can't let this moment end",
    "You set off a dream with me",
    "Getting louder now",
    "Can you hear it echoing?",
    "Take my hand",
    "Will you share this with me?",
    "'Cause darling without you",
    "All the shine of a thousand spotlights",
    "All the stars we steal from the nightsky",
    "Will never be enough",
    "Never be enough",
    "Towers of gold are still too little",
    "These hands could hold the world but it'll",
    "Never be enough", "For me",
    "Never, never", "Never, for me",
    "Never enough", "For me"};
    FILE *fp=fopen("NeverEnough.txt","w");
    narray=sizeof(array)/sizeof(array[0]);
    for(i=0;i<narray-6;i++){
        printf("%s\n",array[i]);
        fputs( array[i], fp );
        fputs("\n",fp);
    }
    for(i=narray-6;i<narray;i++){
        switch(i-(narray-6)){
            case 0:
                loop=2;
                break;
            case 1:
                loop=1;
                break;
            case 2:
                loop=2;
                break;
            case 3:
                loop=1;
                break;
            case 4:
                loop=3;
                break;
            case 5:
                loop=4;
                break;
        }
        for (j=1;j<=loop;j++){
            printf("%s\n",array[i]);
            fputs( array[i], fp );
            fputs("\n",fp);
        }
    }
    fclose(fp);
    FILE *fp2;
    char c;
    fp2=fopen("NeverEnough.txt","r");
    c=fgetc(fp2);
    while(c!=EOF) { printf("%c",c);  c=fgetc(fp2);}
    fclose(fp2);
}
