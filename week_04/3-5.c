// [ Code ] : ex3-5.c (switch…case),( ? : ), goto
#include <stdio.h>
#include <stdlib.h>
int main() {
    int grade;
    char start;
    do{
    printf("輸入一個成績介於 0~100:");
    scanf("%d", &grade);

    switch(grade >= 60? 1:0){ // 判斷成績是否及格
        case 0:
            printf("\n 成績不及格！\n\n");
            break;
        case 1:
            printf("\n 成績及格！恭喜！\n\n");
            break;
 }
    printf("是否要繼續輸入?(y/n):");
    scanf("%s",&start);//這邊用%s不用%c的原因是因為%c會自動抓取上次進行的scanf的Enter鍵產生的\n  如果這邊用%c就會抓到\n  while就會等於false所以會直接跳出do  while
        }while(start=='y');
    //goto START; // 回到 START
    system("PAUSE");
    return 0;
}
