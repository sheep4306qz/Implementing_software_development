#include <iostream>
#include <cstdlib>
using namespace std;
unsigned Fibonacci(unsigned val){
 if(val>1)
    return Fibonacci(val-1)+Fibonacci(val-2);
}
unsigned gcd(int a,int b){
    if(a%b==0)
        return b;
    return gcd(b,a%b);
}
int main(){
 int a,b,fib;
 char yorn;
 cout << "請輸入二個值，計算兩值最大公因數:\n";
 cin >> a;
 cin >> b;
 cout << "GCD(" << a <<","<<b<<")="<<  gcd(a,b);
 cout << "\n請輸入一個值計算其費波那契數:\n";
 cin >> fib;
 for(int i=0;i<=fib;i++){
    cout <<"fib[" <<i<<"]="<<Fibonacci(i)<<endl;
 }
 system("pause");
 return 0;
}
