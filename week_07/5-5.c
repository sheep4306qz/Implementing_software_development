 #include <stdio.h>
 #include <stdlib.h>
 #include <ctype.h>
 #include <conio.h>
 #include <time.h>
 //#include<windows.h>
 void lottery(int max,int min,int num,int *choose){
    int i,j,choice,max_dim;
    int lot[max];
    max_dim = max - min + 1; // 初始設定號碼範圍：在1~49間
    for(i=0;i<max_dim;i++)
    {
        lot[i] = min + i;
    }
    srand((unsigned)time(NULL)); /*亂數種子產生器*/
    //Sleep(1000);
    for(i=0;i<num;i++)
    {
        choice = rand()%max_dim; // 在1~49間：隨機選取號碼
        choose[i] = lot[choice];
        for(j=choice;j<max_dim;j++)
        {
            lot[j]=lot[j+1]; // 剔除已經選中的號碼
        }
    max_dim--;
    }
 }
 void bubbleSort(int *choose){
    int i,j,temp;
    for(j=6; j>1; j--) /* 氣泡排序6個號碼*/
    {
        for(i=0; i<j-1; i++)
        if( choose[i+1] < choose[i] ) {
            temp = choose[i+1];
            choose[i+1] = choose[i];
            choose[i] = temp;
        }
    }
 }
 int main(void)
 {
    int min=1, max=49, num=6;
    //int lot[max];
    int choose[num];
    //int max_dim, choice;
    int i, j;
    char ch;
    do
    {
    for(i=0;i<num;i++) choose[i]=0;
    lottery(max,min,num,choose);
    printf("\n\n ######################################");
    printf("\n\n 本期樂透彩 電腦選號(1-49) 號碼如下：\n\n");
    for(i=0; i<6; i++) /* 印出樂透彩6個號碼 */
    { printf("\t%d", choose[i]); }
    bubbleSort(choose);
    printf("\n\n ==>");
    for(i=0; i<6; i++) /* 印出氣泡排序後的6個樂透彩號碼 */
    {
        printf("\t%d", choose[i]);
    }
    printf("\n\n 是否需要另一組 電腦選號 號碼？(y/n)：");
    ch = getche();
    ch = toupper(ch);
    }while(ch=='Y');
    printf("\n\n");
    system("PAUSE");
    return 0;
    }
