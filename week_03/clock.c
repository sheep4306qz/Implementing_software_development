#include<stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <windows.h>
int main(){
    int s,m,h;//s為秒，m為分，h為小時
    printf("請輸入開始時間為何時(hh:mm:ss)，此時鐘為24小時制。EX 13:45:01\n");
    re:scanf("%d:%d:%d",&h,&m,&s);
    if (!(h>=0&&h<24)||!(m>=0&&m<60)||!(s>=0&&s<60))  //判斷使用者輸入的時間是否合理
    {
        printf("時間輸入格式錯誤，請重新輸入\n");
        goto re;
    }
    while(1){
        for (;s<=60;s++){
            printf("\r");
            if(s==60){
                m++;
                s=0;
            }
            if(m==60){
                h++;
                m=0;
            }
            if(h==24){
                h=0;
                m=0;
                s=0;
            }
            Sleep(1000);//延遲1秒
            printf("時間  %.2d:%.2d:%.2d",h,m,s);
        }
    }

    return 0;
}
