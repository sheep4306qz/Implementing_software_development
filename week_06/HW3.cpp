// [ Code ]: sqrt2.cpp - C++µ{¦¡
#include <iostream>
#include <cstdlib>
#include <math.h>
#include <iomanip>      // std::setprecision
using namespace std;

int main(){
    double a, middle, lower, upper;
    double err;
    //int count;
    double b;
    cout << "請輸入數值:" << endl;
    cin >> a;
    cout << "請輸入次方根:" << endl;
    cin >> b;

    err = 0.0001;
    lower = 0;
    upper = a;
    if(a < 1) upper = 1.0 + a;

    //count = 0;
    while(1){
        //count++;
        middle = (lower + upper)/2.0;
        if(a < pow(middle,b))
            upper = middle;
        else
            lower = middle;
        if((upper - lower) < err)
            break;
    }
    cout << "The upper value = " << upper
                << "\nThe lower value = " << lower
                << "\n數值" << a
                <<"的" << b
                << "次方根 = " << setprecision(4) << upper <<endl;
                //<< "\n count = " << count <<endl;
    return 0;
}
