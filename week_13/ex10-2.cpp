// [ Code ]: ex10-2.cpp  Constructor Loading
#include <iostream>
#include <cstdlib>
using namespace std;
class Fibonacci{
private:
 int arraySize;
 int fib0, fib1;
 int *pf;
public:
 Fibonacci();
 Fibonacci(int);
 Fibonacci(int, int, int);
 int fibo();
 ~Fibonacci(){
 delete [] pf;
 cout << "\nArraySize =" << arraySize
 << "秆篶 ~Fibonacci()砆㊣磅︽\n\n"; }
};
Fibonacci::Fibonacci(){ // 箇砞篶
 arraySize = 5; // 箇砞皚 5
 fib0 = 1; // 箇砞 fib0 = 1
 fib1 = 1; // 箇砞 fib1 = 1
 }
Fibonacci::Fibonacci(int size){
 // 狦﹍て size ぃ单箂玥箇砞皚 10
 arraySize = ((size > 0)? size : 10);
 fib0 = 1; // 箇砞 fib0 = 1
 fib1 = 1; // 箇砞 fib1 = 1
 }
Fibonacci::Fibonacci(int size, int f0, int f1) {
 // 狦﹍て size ぃ单箂玥箇砞皚 10
 arraySize = ((size > 0)? size : 10);
 fib0 = f0; // change fib0 to f0
 fib1 = f1; // change fib1 to f1
 }
 int Fibonacci::fibo(){
 pf = new int [arraySize];
 *pf = fib0;
 *(pf+1) = fib1;
 for(int i=0; i < (arraySize-2); i++)
 { pf[i+2] = pf[i+1] + pf[i]; }
 cout << "玡" << arraySize << "禣猧ê计";
 for(int i=0; i<arraySize; i++) cout << '\0' << pf[i];
 cout << endl;
 return 0;
}
int main(){
 Fibonacci *pfi1, fi1, fi2(8), fi3(0,2,4);
 /* 把酚(reference,ㄒ:&pfi2)ゲ斗﹍て
 pfi2 单 fi1碞钩琌 fi2 (alias) */
 Fibonacci &pfi2 = fi2;
 fi1.fibo();
 (&fi1)->fibo();
 fi2.fibo();
 pfi2.fibo();
 pfi1 = &fi3;
 pfi1->fibo(); // fi3.fibo();
 fi2.~Fibonacci();
 system("PAUSE");
 return 0;
}
