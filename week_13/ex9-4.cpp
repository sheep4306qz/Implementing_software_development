// Example 9-4: this Pointer
// [ Code ] : p_this.cpp
#include <iostream>
using namespace std;
class P_this {
 int a;
 public: // 如果改成 P_this seta(int a){ a = a; }; 會發生什麼事？------程式會分不清楚這兩個a誰是誰，所以要用指向運算指去分別
     //所以this->a=a;  前面的this->a的a是此類別中的a也就是上一行中int a的a，後面那個a則是傳進來的值，簡單寫的話直接寫兩個不同的變數名就不會有這個問題了。
 P_this seta(int a){
 this->a = a;
 return *this;
}
void print(){cout << a;}
};
int main(){
 P_this pt1, pt2;
 pt2.seta(100);
 pt2.print();
 return 0;
}
